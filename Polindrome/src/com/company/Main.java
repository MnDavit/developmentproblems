package com.company;

public class Main {

    public static void main(String[] args) {
        int r;
        int sum = 0;
        int compare;
        int n = 435252534;

        compare = n;
        while(n > 0){
            r = n % 10;
            sum = (sum * 10) + r;
            n = n / 10;
        }
        if(compare == sum)
            System.out.println("palindrome number ");
        else
            System.out.println("not palindrome");
    }
}
